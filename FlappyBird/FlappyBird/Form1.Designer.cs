﻿
namespace FlappyBird
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
			this.FlappyBird = new System.Windows.Forms.PictureBox();
			this.GravityTimer = new System.Windows.Forms.Timer(this.components);
			this.JumpTimer = new System.Windows.Forms.Timer(this.components);
			this.JumpWaitTimer = new System.Windows.Forms.Timer(this.components);
			this.Sounds = new AxWMPLib.AxWindowsMediaPlayer();
			this.Ground = new System.Windows.Forms.PictureBox();
			this.TopPipe = new System.Windows.Forms.PictureBox();
			this.BottomPipe = new System.Windows.Forms.PictureBox();
			this.Coin = new System.Windows.Forms.PictureBox();
			this.Score_Label = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.FlappyBird)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Sounds)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Ground)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.TopPipe)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.BottomPipe)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.Coin)).BeginInit();
			this.SuspendLayout();
			// 
			// FlappyBird
			// 
			this.FlappyBird.BackColor = System.Drawing.Color.Transparent;
			this.FlappyBird.Image = ((System.Drawing.Image)(resources.GetObject("FlappyBird.Image")));
			this.FlappyBird.Location = new System.Drawing.Point(51, 176);
			this.FlappyBird.Name = "FlappyBird";
			this.FlappyBird.Size = new System.Drawing.Size(49, 42);
			this.FlappyBird.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.FlappyBird.TabIndex = 0;
			this.FlappyBird.TabStop = false;
			// 
			// GravityTimer
			// 
			this.GravityTimer.Interval = 10;
			this.GravityTimer.Tick += new System.EventHandler(this.GravityTimer_Tick);
			// 
			// JumpTimer
			// 
			this.JumpTimer.Interval = 10;
			this.JumpTimer.Tick += new System.EventHandler(this.JumpTimer_Tick);
			// 
			// JumpWaitTimer
			// 
			this.JumpWaitTimer.Interval = 10;
			this.JumpWaitTimer.Tick += new System.EventHandler(this.JumpWaitTimer_Tick);
			// 
			// Sounds
			// 
			this.Sounds.Enabled = true;
			this.Sounds.Location = new System.Drawing.Point(12, 425);
			this.Sounds.Name = "Sounds";
			this.Sounds.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Sounds.OcxState")));
			this.Sounds.Size = new System.Drawing.Size(75, 23);
			this.Sounds.TabIndex = 1;
			this.Sounds.Visible = false;
			// 
			// Ground
			// 
			this.Ground.BackColor = System.Drawing.Color.Transparent;
			this.Ground.Image = ((System.Drawing.Image)(resources.GetObject("Ground.Image")));
			this.Ground.Location = new System.Drawing.Point(-15, 392);
			this.Ground.Name = "Ground";
			this.Ground.Size = new System.Drawing.Size(519, 136);
			this.Ground.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
			this.Ground.TabIndex = 2;
			this.Ground.TabStop = false;
			// 
			// TopPipe
			// 
			this.TopPipe.BackColor = System.Drawing.Color.Transparent;
			this.TopPipe.Image = ((System.Drawing.Image)(resources.GetObject("TopPipe.Image")));
			this.TopPipe.Location = new System.Drawing.Point(428, -143);
			this.TopPipe.Name = "TopPipe";
			this.TopPipe.Size = new System.Drawing.Size(53, 267);
			this.TopPipe.TabIndex = 3;
			this.TopPipe.TabStop = false;
			this.TopPipe.Visible = false;
			// 
			// BottomPipe
			// 
			this.BottomPipe.BackColor = System.Drawing.Color.Transparent;
			this.BottomPipe.Image = ((System.Drawing.Image)(resources.GetObject("BottomPipe.Image")));
			this.BottomPipe.Location = new System.Drawing.Point(429, 250);
			this.BottomPipe.Name = "BottomPipe";
			this.BottomPipe.Size = new System.Drawing.Size(52, 241);
			this.BottomPipe.TabIndex = 4;
			this.BottomPipe.TabStop = false;
			this.BottomPipe.Visible = false;
			// 
			// Coin
			// 
			this.Coin.Image = ((System.Drawing.Image)(resources.GetObject("Coin.Image")));
			this.Coin.Location = new System.Drawing.Point(424, 151);
			this.Coin.Name = "Coin";
			this.Coin.Size = new System.Drawing.Size(64, 64);
			this.Coin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
			this.Coin.TabIndex = 5;
			this.Coin.TabStop = false;
			this.Coin.Visible = false;
			// 
			// Score_Label
			// 
			this.Score_Label.AutoSize = true;
			this.Score_Label.BackColor = System.Drawing.Color.Transparent;
			this.Score_Label.Font = new System.Drawing.Font("Lucida Console", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.Score_Label.Location = new System.Drawing.Point(123, 425);
			this.Score_Label.Name = "Score_Label";
			this.Score_Label.Size = new System.Drawing.Size(124, 27);
			this.Score_Label.TabIndex = 6;
			this.Score_Label.Text = "Score: ";
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
			this.ClientSize = new System.Drawing.Size(493, 470);
			this.Controls.Add(this.Score_Label);
			this.Controls.Add(this.Coin);
			this.Controls.Add(this.TopPipe);
			this.Controls.Add(this.Sounds);
			this.Controls.Add(this.FlappyBird);
			this.Controls.Add(this.Ground);
			this.Controls.Add(this.BottomPipe);
			this.Name = "Form1";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "FlappyBird";
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
			((System.ComponentModel.ISupportInitialize)(this.FlappyBird)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Sounds)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Ground)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.TopPipe)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.BottomPipe)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.Coin)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.PictureBox FlappyBird;
		private System.Windows.Forms.Timer GravityTimer;
		private System.Windows.Forms.Timer JumpTimer;
		private System.Windows.Forms.Timer JumpWaitTimer;
		private AxWMPLib.AxWindowsMediaPlayer Sounds;
		private System.Windows.Forms.PictureBox Ground;
		private System.Windows.Forms.PictureBox TopPipe;
		private System.Windows.Forms.PictureBox BottomPipe;
		private System.Windows.Forms.PictureBox Coin;
		private System.Windows.Forms.Label Score_Label;
	}
}

