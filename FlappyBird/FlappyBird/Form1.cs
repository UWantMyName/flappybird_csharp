﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Media;
using System.Windows.Forms;

namespace FlappyBird
{
	public partial class Form1 : Form
	{
		private int gravity = 3; // how fast it falls
		private int jumpSpeed = -4; // how high it jumps
		private int miliseconds = 0; // to know when to stop the jump timer

		private int pipeSpeed = -10;

		private int Score = 0;

		private Image frame1;
		private Image frame2;
		private Image frame3;

		private bool dead = false;

		private string path_jump_sound = @"D:\STEVEN STUFF\TEACHING\SOUMYA\Lesson 39 - 13 February 2021\flappybird_csharp\FlappyBird\FlappyBird\Sound Effects\wing.wav";
		private string path_die_sound = @"D:\STEVEN STUFF\TEACHING\SOUMYA\Lesson 39 - 13 February 2021\flappybird_csharp\FlappyBird\FlappyBird\Sound Effects\hit.wav";

		private PictureBox pipeTop;
		private PictureBox coin;
		private PictureBox pipeBot;
		private bool gotCoin = false;

		private BackgroundWorker PipeTimer = new BackgroundWorker();

		private int X_pipes = 0;
		private bool pipesBehind = false;

		private int deltaHeight = 0;

		public Form1()
		{
			InitializeComponent();

			frame1 = Image.FromFile(@"D:\STEVEN STUFF\TEACHING\SOUMYA\Lesson 39 - 13 February 2021\Sprites\ACTUAL Flappy Bird\FB_0.png");
			frame2 = Image.FromFile(@"D:\STEVEN STUFF\TEACHING\SOUMYA\Lesson 39 - 13 February 2021\Sprites\ACTUAL Flappy Bird\FB_1.png");
			frame3 = Image.FromFile(@"D:\STEVEN STUFF\TEACHING\SOUMYA\Lesson 39 - 13 February 2021\Sprites\ACTUAL Flappy Bird\FB_2.png");
			GravityTimer.Start();

			PipeTimer.DoWork += new DoWorkEventHandler(PipeTimer_Work);
			SpawnPipes();
		}

		private bool CheckBirdCollisionWithPipes()
		{
			// We need the bottom Y Axis of the pipeTop
			// And the top Y Axis of the pipeBot

			Point p1 = pipeTop.Location;
			float Y1 = p1.Y + pipeTop.Size.Height;
			float X1_L = p1.X;
			float X1_R = p1.X + pipeTop.Size.Width;

			Point p2 = pipeBot.Location;
			float Y2 = p2.Y;

			// Calculating the top Y axis and the bot Y axis of the bird
			Point P = FlappyBird.Location;
			float Y_T = P.Y;
			float Y_B = P.Y + FlappyBird.Size.Height;
			float X_L = P.X;
			float X_R = P.X + FlappyBird.Size.Width;

			if ((X_R < X1_L && X_R < X1_R) || (X1_R < X_L && X1_L < X_L)) return false;
			else if ((Y1 < Y_T) && (Y_B < Y2)) return false;
			return true;
		}

		private bool CheckBirdCollisionWithCoin()
		{
			Point birdLocation = FlappyBird.Location;
			Point coinLocation = coin.Location;

			float X_Right = birdLocation.X + FlappyBird.Size.Width;
			float X_Left = birdLocation.X;
			float X_Right_Coin = coinLocation.X + coin.Size.Width;
			float X_Left_Coin = coinLocation.X;

			float Y_Top = birdLocation.Y;
			float Y_Bot = birdLocation.Y + FlappyBird.Size.Height;
			float Y_Bot_Coin = coinLocation.Y + coin.Size.Width;
			float Y_Top_Coin = coinLocation.Y;

			if ((X_Left_Coin <= X_Left && X_Left <= X_Right_Coin) || (X_Left_Coin <= X_Right && X_Right <= X_Right_Coin))
				if ((Y_Top_Coin <= Y_Top && Y_Top <= Y_Bot_Coin) || (Y_Top_Coin <= Y_Bot && Y_Bot <= Y_Bot_Coin))
					return true;
			return false;
		}

		private void GameOver()
		{
			SoundPlayer player = new SoundPlayer(path_die_sound);
			player.Play();

			GravityTimer.Stop();
			dead = true;
			gravity = 0;
		}

		private void PipeTimer_Work(object sender, DoWorkEventArgs e)
		{
			while (!dead)
			{
				if (!pipesBehind)
				{
					/// move them..
					Point p = pipeBot.Location;
					p.X += pipeSpeed;
					pipeBot.Location = p;

					p = pipeTop.Location;
					p.X += pipeSpeed;
					pipeTop.Location = p;

					p = coin.Location;
					p.X += pipeSpeed;
					coin.Location = p;

					X_pipes = pipeTop.Location.X;
					UpdatePipes();
				}
				else RespawnPipes();

				System.Threading.Thread.Sleep(60);
			}
		}

		private void SpawnPipes()
		{
			pipeTop = TopPipe;
			pipeTop.Visible = true;
			pipeBot = BottomPipe;
			pipeBot.Visible = true;
			coin = Coin; // i am borrowing the properties of the coin picturebox
			coin.Visible = true;

			X_pipes = pipeTop.Location.X + pipeTop.Size.Width;

			this.Controls.Add(pipeTop);
			this.Controls.Add(coin);
			this.Controls.Add(pipeBot);

			PipeTimer.RunWorkerAsync();
		}

		private void Form1_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyCode == Keys.Space && !dead)
			{
				SoundPlayer player = new SoundPlayer(path_jump_sound);
				player.Play();

				miliseconds = 0;
				GravityTimer.Stop();
				JumpTimer.Start();	
			}
		}

		private void UpdatePipes()
		{
			if (X_pipes <= -50)
				pipesBehind = true;
		}

		private void RespawnPipes()
		{
			pipesBehind = false;

			this.Controls.Remove(pipeTop);
			this.Controls.Remove(pipeBot);

			Random r = new Random();
			deltaHeight = r.Next(-100, 100);

			pipeTop.Location = new Point(478, -143 + deltaHeight);
			pipeBot.Location = new Point(479, 250 + deltaHeight);
			coin.Location = new Point(474, 151 + deltaHeight);

			gotCoin = false;

			this.Controls.Add(pipeTop);
			this.Controls.Add(pipeBot);
			this.Controls.Add(coin);
		}

		private void GravityTimer_Tick(object sender, EventArgs e)
		{
			if (!dead)
			{
				Point p = FlappyBird.Location;

				// checking if the bird hit the ground
				float Y_bottom_bird = p.Y + FlappyBird.Size.Height;
				Point p1 = Ground.Location;
				float Y_top_ground = p1.Y;

				if (Y_bottom_bird >= Y_top_ground && !dead) GameOver();
				else if (CheckBirdCollisionWithPipes() && !dead) GameOver();
				else
				{
					// fall down
					p = FlappyBird.Location;
					p.Y += gravity;
					FlappyBird.Location = p;

					if (CheckBirdCollisionWithCoin() && !gotCoin)
					{
						gotCoin = true;
						this.Controls.Remove(coin);
						Score++;
						Score_Label.Text = "Score: " + Score.ToString();
						// I should also play a sound, but we will implement it later.
					}
				}
			}
		}

		private void JumpTimer_Tick(object sender, EventArgs e)
		{
			if (CheckBirdCollisionWithPipes() && !dead) GameOver();

			if (miliseconds == 50) FlappyBird.Image = frame1;

			if (CheckBirdCollisionWithCoin() && !gotCoin)
			{
				gotCoin = true;
				this.Controls.Remove(coin);
				Score++;
				Score_Label.Text = "Score: " + Score.ToString();
				// I should also play a sound, but we will implement it later.
			}

			if (miliseconds < 100)
			{
				miliseconds += JumpTimer.Interval;
				Point p = FlappyBird.Location;
				p.Y += jumpSpeed;
				FlappyBird.Location = p;
			}
			else
			{
				JumpTimer.Stop();
				JumpWaitTimer.Start();
				FlappyBird.Image = frame2;
			}
		}

		private void JumpWaitTimer_Tick(object sender, EventArgs e)
		{
			if (CheckBirdCollisionWithPipes() && !dead) GameOver();

			if (CheckBirdCollisionWithCoin() && !gotCoin)
			{
				gotCoin = true;
				this.Controls.Remove(coin);
				Score++;
				Score_Label.Text = "Score: " + Score.ToString();
				// I should also play a sound, but we will implement it later.
			}

			if (miliseconds < 140)
			{
				miliseconds += JumpWaitTimer.Interval;
			}
			else
			{
				JumpWaitTimer.Stop();
				GravityTimer.Start();
				FlappyBird.Image = frame3;
			}
		}
	}
}
